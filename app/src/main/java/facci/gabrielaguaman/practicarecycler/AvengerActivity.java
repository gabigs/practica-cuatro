package facci.gabrielaguaman.practicarecycler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class AvengerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avenger);

        RecyclerView recyclerViews = findViewById(R.id.recycler_avenger);

        ArrayList<String> avng = new ArrayList<>();

        for (int item = 0; item <= 10; item ++){
            avng.add("Thor");
            avng.add("Iron Man");
            avng.add("Black Widow");
            avng.add("Hawkeye");
            avng.add("Loki");
            avng.add("Bruce Banner");
            avng.add("Wanda Maximoff");
            avng.add("Captain America");
            avng.add("Spiderman");
            avng.add("Vision");
        }
        TareasRecyclerViewAdapters adapte = new TareasRecyclerViewAdapters(this, avng);

        recyclerViews.setAdapter(adapte);
        recyclerViews.setLayoutManager(new LinearLayoutManager(  this));
    }
}