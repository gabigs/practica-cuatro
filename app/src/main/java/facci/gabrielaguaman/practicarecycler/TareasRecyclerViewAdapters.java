package facci.gabrielaguaman.practicarecycler;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class TareasRecyclerViewAdapters extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> items;
    Typeface fontBold;
    Context mContext;

    private final int ITEMA = 0, TITLEA = 1;

    public TareasRecyclerViewAdapters(Context mContext, ArrayList<String> items) {
        this.items = items;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (i){
            case ITEMA:
                View vv1 = inflater.inflate(R.layout.recycler_itema, viewGroup,false);
                viewHolder = new ViewHolderItem(vv1);
                break;
            case TITLEA:
                View vv2 = inflater.inflate(R.layout.recycler_titlea,viewGroup,false);
                viewHolder = new ViewHolderTitle(vv2);

                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()){

            case ITEMA:
                ViewHolderItem vhh = (ViewHolderItem)viewHolder;
                configureViewHolderTitle(vhh, i);

                break;
            case TITLEA:
                ViewHolderTitle vhhh = (ViewHolderTitle)viewHolder;
                configureViewHolderItem(vhhh, i);
                break;
        }

    }


    private void configureViewHolderTitle(ViewHolderItem vh1, int i){
        String task = items.get(i);
        if (task != null){
            vh1.getTaskName().setText(task);
            vh1.getTextStatus().setText("TAREA");
        }
    }

    private void configureViewHolderItem(ViewHolderTitle vh2, int i){
        String separatorString = items.get(i);
        vh2.getTextViewSeparator().setText(separatorString);
        vh2.getTextViewSeparator().setTypeface(fontBold);
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder implements
            View.OnClickListener{

        private TextView textViewNombreTarea, textViewEstadoTarea;

        public ViewHolderItem(@NonNull View itemView) {
            super(itemView);
            textViewEstadoTarea = (TextView) itemView.findViewById(R.id.task_statusa);
            textViewNombreTarea = (TextView) itemView.findViewById(R.id.task_namea);
            itemView.setOnClickListener(this);
        }

        public TextView getTaskName(){
            return textViewNombreTarea;
        }

        public void setTaskName(TextView label1){
            this.textViewNombreTarea = label1;
        }

        public TextView getTextStatus(){
            return  textViewEstadoTarea;
        }

        public void seTextStatus(TextView label2){
            this.textViewEstadoTarea = label2;
        }


        @Override
        public void onClick(View v) {
            int i = getLayoutPosition();
        }
    }

    public class ViewHolderTitle extends RecyclerView.ViewHolder{
        private TextView textViewTitulo;

        public ViewHolderTitle(@NonNull View view) {
            super(view);
            textViewTitulo = view.findViewById(R.id.separadors);
        }

        public  TextView getTextViewSeparator(){
            return textViewTitulo;
        }

        public TextView setTextViewSeparator(TextView separator){
            return textViewTitulo = separator;
        }
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return TITLEA;
        }else {
            return ITEMA;
        }
    }
}
